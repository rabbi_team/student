/*
Navicat MySQL Data Transfer

Source Server         : test
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : student_manager1

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2018-07-07 20:53:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `rol`
-- ----------------------------
DROP TABLE IF EXISTS `rol`;
CREATE TABLE `rol` (
  `r_id` int(11) NOT NULL AUTO_INCREMENT,
  `r_name` varchar(50) NOT NULL,
  `r_status` enum('1','2') DEFAULT '1',
  PRIMARY KEY (`r_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rol
-- ----------------------------
INSERT INTO `rol` VALUES ('1', '超级管理员', '1');

-- ----------------------------
-- Table structure for `tb_limit`
-- ----------------------------
DROP TABLE IF EXISTS `tb_limit`;
CREATE TABLE `tb_limit` (
  `l_id` int(11) NOT NULL AUTO_INCREMENT,
  `l_name` varchar(50) NOT NULL,
  `l_m_id` int(11) NOT NULL,
  `l_url` varchar(255) NOT NULL,
  `l_status` enum('1','2') DEFAULT '1',
  PRIMARY KEY (`l_id`),
  KEY `l_m_id` (`l_m_id`),
  CONSTRAINT `tb_limit_ibfk_1` FOREIGN KEY (`l_m_id`) REFERENCES `tb_module` (`m_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_limit
-- ----------------------------
INSERT INTO `tb_limit` VALUES ('1', '查询', '1', 'Module/selectAllindex', '1');
INSERT INTO `tb_limit` VALUES ('2', '添加', '1', 'Module/insert', '1');
INSERT INTO `tb_limit` VALUES ('3', '修改', '1', 'Module/update', '1');
INSERT INTO `tb_limit` VALUES ('4', '删除', '1', 'Module/delete', '1');
INSERT INTO `tb_limit` VALUES ('5', '查询', '4', 'Rol/selectAll', '1');
INSERT INTO `tb_limit` VALUES ('6', '修改', '4', 'Rol/update', '1');
INSERT INTO `tb_limit` VALUES ('7', '删除', '4', 'Rol/delete', '1');
INSERT INTO `tb_limit` VALUES ('8', '添加', '4', 'Rol/insert', '1');
INSERT INTO `tb_limit` VALUES ('9', '查询', '5', 'User/selectAll', '1');
INSERT INTO `tb_limit` VALUES ('10', '添加', '5', 'User/insert', '1');
INSERT INTO `tb_limit` VALUES ('11', '修改', '5', 'User/update', '1');
INSERT INTO `tb_limit` VALUES ('12', '删除', '5', 'User/delete', '1');
INSERT INTO `tb_limit` VALUES ('13', '查询', '3', 'login', '1');

-- ----------------------------
-- Table structure for `tb_module`
-- ----------------------------
DROP TABLE IF EXISTS `tb_module`;
CREATE TABLE `tb_module` (
  `m_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_name` varchar(50) NOT NULL,
  `m_url` varchar(255) NOT NULL,
  `m_status` enum('1','2') DEFAULT '1',
  PRIMARY KEY (`m_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_module
-- ----------------------------
INSERT INTO `tb_module` VALUES ('1', '模块管理', 'Module/selectAllindex', '1');
INSERT INTO `tb_module` VALUES ('3', '退出登录', '/login', '1');
INSERT INTO `tb_module` VALUES ('4', '角色管理', 'Rol/selectAll', '1');
INSERT INTO `tb_module` VALUES ('5', '用户管理', 'User/selectAll', '1');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_username` varchar(100) NOT NULL,
  `u_password` varchar(255) NOT NULL,
  `u_name` varchar(50) NOT NULL,
  `u_gender` enum('1','2') DEFAULT '1',
  `u_r_id` int(11) NOT NULL,
  `u_status` enum('1','2') DEFAULT '1',
  PRIMARY KEY (`u_id`),
  UNIQUE KEY `u_username` (`u_username`),
  KEY `u_r_id` (`u_r_id`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`u_r_id`) REFERENCES `rol` (`r_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'z173391478', '123456', '张路平', '1', '1', '1');

-- ----------------------------
-- Table structure for `user_rol_modual_limit`
-- ----------------------------
DROP TABLE IF EXISTS `user_rol_modual_limit`;
CREATE TABLE `user_rol_modual_limit` (
  `urml_id` int(11) NOT NULL AUTO_INCREMENT,
  `urml_u_id` int(11) NOT NULL,
  `urml_r_id` int(11) NOT NULL,
  `urml_m_id` int(11) NOT NULL,
  `urml_l_id` int(11) NOT NULL,
  `urml_status` enum('1','2') NOT NULL,
  PRIMARY KEY (`urml_id`),
  KEY `urml_u_id` (`urml_u_id`),
  KEY `urml_r_id` (`urml_r_id`),
  KEY `urml_m_id` (`urml_m_id`),
  KEY `urml_l_id` (`urml_l_id`),
  CONSTRAINT `user_rol_modual_limit_ibfk_1` FOREIGN KEY (`urml_u_id`) REFERENCES `user` (`u_id`),
  CONSTRAINT `user_rol_modual_limit_ibfk_2` FOREIGN KEY (`urml_r_id`) REFERENCES `rol` (`r_id`),
  CONSTRAINT `user_rol_modual_limit_ibfk_3` FOREIGN KEY (`urml_m_id`) REFERENCES `tb_module` (`m_id`),
  CONSTRAINT `user_rol_modual_limit_ibfk_4` FOREIGN KEY (`urml_l_id`) REFERENCES `tb_limit` (`l_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_rol_modual_limit
-- ----------------------------
INSERT INTO `user_rol_modual_limit` VALUES ('1', '1', '1', '1', '1', '1');
INSERT INTO `user_rol_modual_limit` VALUES ('2', '1', '1', '1', '2', '1');
INSERT INTO `user_rol_modual_limit` VALUES ('3', '1', '1', '1', '3', '1');
INSERT INTO `user_rol_modual_limit` VALUES ('4', '1', '1', '1', '4', '1');
INSERT INTO `user_rol_modual_limit` VALUES ('5', '1', '1', '4', '5', '1');
INSERT INTO `user_rol_modual_limit` VALUES ('6', '1', '1', '4', '6', '1');
INSERT INTO `user_rol_modual_limit` VALUES ('7', '1', '1', '4', '7', '1');
INSERT INTO `user_rol_modual_limit` VALUES ('8', '1', '1', '4', '8', '1');
INSERT INTO `user_rol_modual_limit` VALUES ('9', '1', '1', '5', '9', '1');
INSERT INTO `user_rol_modual_limit` VALUES ('10', '1', '1', '5', '10', '1');
INSERT INTO `user_rol_modual_limit` VALUES ('11', '1', '1', '5', '11', '1');
INSERT INTO `user_rol_modual_limit` VALUES ('12', '1', '1', '5', '12', '1');
INSERT INTO `user_rol_modual_limit` VALUES ('13', '1', '1', '3', '13', '1');
