<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="${pageContext.request.contextPath}/recourses/js/jsutil.js"></script>
<script src="${pageContext.request.contextPath}/recourses/js/infoSet.js"></script>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">
			角色管理
			<button type="button" class="btn btn-outline btn-success"
				data-toggle="modal" data-target="#chara">添加角色</button>
		</h1>
		<div style="margin-bottom: auto" class="modal fade" id="chara"
			tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
			aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form id="addForm">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">添加角色</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<h4 class="modal-title" id="myModalLabel">角色名称</h4>
								<input class="form-control" name="rName">
							</div>
							<div class="form-group">
								<h4 class="modal-title" id="myModalLabel">可用状态</h4>
								<div class="form-group">
									<select class="form-control" name="rStatus">
										<option value="1">可用</option>
										<option value="2">不可用</option>
									</select>
								</div>
							</div>
						</div>
					</form>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button type="button" class="btn btn-primary" id="addBtn">提交更改</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$("#addBtn").click(function() {
		$.ajax({
			type : "post",
			url : "${pageContext.request.contextPath}/${empty limitMap.insert ? '':limitMap.insert.lUrl}",
			data : $("#addForm").serialize(),
			success : function(data) {
				if (data == 'success') {
					alert("添加角色成功");
					location.reload();
				} else
					alert("添加角色失败");
			}
		});
	});
</script>
<div style="margin-bottom: auto" class="modal fade" id="updModal"
	tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">修改角色</h4>
			</div>
			<div class="modal-body">
				<form id="updForm">
					<input class="form-control" type="hidden" id="updRId" name="rId">
					<div class="form-group">
						<h4 class="modal-title" id="myModalLabel">角色名称</h4>
						<input class="form-control" name="rName" id="updRName">
					</div>
					<div class="form-group">
						<h4 class="modal-title" id="myModalLabel">可用状态</h4>
						<div class="form-group">
							<select class="form-control" id="role" name="rStatus">
								<option value="1">可用</option>
								<option value="2">不可用</option>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="updBtn">提交更改</button>
			</div>
		</div>
	</div>
</div>
<script>
	$("#updBtn").click(function() {
		$.ajax({
			type : "post",
			url : "${pageContext.request.contextPath}/${empty limitMap.update ? '':limitMap.update.lUrl}",
			data : $("#updForm").serialize(),
			success : function(data) {
				if (data == 'isUse')
					alert("该角色已经有用户在使用");
				else if (data == "success") {
					alert("修改成功");
					location.reload();
				} else
					alert("修改失败");
			}
		});
	});
</script>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-body">
			<table width="100%;"
				class="table table-striped table-bordered table-hover table-responsive text-center">
				<thead>
					<tr>
						<th>操作</th>
						<th>角色名</th>
						<th>可用状态</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pageInfo.list }" var="rol">
						<tr>
							<td><button type="button" class="btn btn-outline btn-info"
									onclick=<c:if test="${empty limitMap.update }">
									"error()"
									</c:if>
									<c:if test="${not empty limitMap.update }"> 
									"selectOne(${rol.rId })"
									</c:if>>修改</button>
								<button type="button" class="btn btn-outline btn-danger"
									onclick=<c:if test="${empty limitMap.delete }">
									"error()"
									</c:if>
									<c:if test="${not empty limitMap.delete }">
									"deleteOne(${rol.rId })"
									</c:if>>删除</button>
							<td><p>${rol.rName }</p></td>
							<td><p>${rol.rStatus eq '1'?'可用':'不可用' }</p></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<ul class="pagination">
				<c:if test="${pageInfo.hasPreviousPage}">
					<li><a
						href="${pageContext.request.contextPath}/Rol/selectAll?curPage=${pageInfo.prePage }">&laquo;</a></li>
				</c:if>
				<c:forEach items="${pageInfo.navigatepageNums }" var="pageNumber">
					<li
						<c:if  test="${pageNumber eq pageInfo.pageNum }">
							class="active"
						</c:if>><a
						<c:if test="${pageNumber != pageInfo.pageNum }">					
						href="${pageContext.request.contextPath}/Rol/selectAll?curPage=${pageNumber }"
						</c:if>>${pageNumber }</a></li>
				</c:forEach>
				<c:if test="${pageInfo.hasNextPage }">
					<li><a
						href="${pageContext.request.contextPath}/Rol/selectAll?curPage=${pageInfo.nextPage }">&raquo;</a></li>
				</c:if>
			</ul>
		</div>
	</div>
</div>
<script>
	function selectOne(rIdVar) {
		$.ajax({
			type : "post",
			url : "${pageContext.request.contextPath}/Rol/selectOne",
			data : {
				rId : rIdVar
			},
			success : function(data) {
				for (curKey in data) {
					if (curKey == 'rStatus')
						continue;
					$("#upd" + title(curKey)).val(data[curKey]);
				}
				setOptionSelectedByValue("role", data.rStatus);
				$("#updModal").modal();
			}
		});
	}
	function deleteOne(rIdVar) {
		$.ajax({
			type : "post",
			url : "${pageContext.request.contextPath}/${empty limitMap.delete ? '':limitMap.delete.lUrl}",
			data : {
				rId : rIdVar
			},
			success : function(data) {
				if (data == 'isUse')
					alert("该角色已经有用户在使用");
				else if (data == "success") {
					alert("删除成功");
					location.reload();
				} else
					alert("删除失败");
			}
		});
	}
	function error(){
		alert("你所属用户的权限无法使用该功能");
	}
</script>