<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>登录</title>
<link
	href="${pageContext.request.contextPath}/recourses/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/recourses/vendor/metisMenu/metisMenu.min.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/recourses/dist/css/sb-admin-2.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/recourses/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">


</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-panel panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">用户登录</h3>
					</div>
					<div class="panel-body">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="登录名" id="username">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="登录密码" type="password"
									id="passwrod">
							</div>
							<button id="btn" class="btn btn-lg btn-success btn-block">登录</button>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/recourses/js/jsutil.js"></script>
<script
	src="${pageContext.request.contextPath}/recourses/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script
	src="${pageContext.request.contextPath}/recourses/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script
	src="${pageContext.request.contextPath}/recourses/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script
	src="${pageContext.request.contextPath}/recourses/dist/js/sb-admin-2.js"></script>
<script>
	$("#btn").click(function() {
		var selectVar = getOptionValueBySelected("role");
		var goPage = "${pageContext.request.contextPath}";
		var paramData = {
			username : "",
			password : ""
		};
		var successPage = "${pageContext.request.contextPath}";
		paramData.username = $("#username").val();
		paramData.password = $("#passwrod").val();
		goPage += "/User/login";
		successPage += "/indexSuperadmin";
		$.ajax({
			type : "post",
			url : goPage,
			data : paramData,
			success : function(data) {
				if (data == '1') {
					alert('登录成功')
					location = successPage;
				} else if (data == '2')
					alert("用户名不存在");
				else
					alert("密码错误");
			}
		});
	});
</script>
</html>