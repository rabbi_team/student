<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>index</title>
<!-- Bootstrap Core CSS -->
<link
	href="${pageContext.request.contextPath}/recourses/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<!-- MetisMenu CSS -->
<link
	href="${pageContext.request.contextPath}/recourses/vendor/metisMenu/metisMenu.min.css"
	rel="stylesheet">
<!-- Custom CSS -->
<link
	href="${pageContext.request.contextPath}/recourses/dist/css/sb-admin-2.css"
	rel="stylesheet">
<!-- DataTables CSS -->
<link
	href="${pageContext.request.contextPath}/recourses/vendor/datatables-plugins/dataTables.bootstrap.css"
	rel="stylesheet">
<!-- DataTables Responsive CSS -->
<link
	href="${pageContext.request.contextPath}/recourses/vendor/datatables-responsive/dataTables.responsive.css"
	rel="stylesheet">
<!-- Morris Charts CSS -->
<link
	href="${pageContext.request.contextPath}/recourses/vendor/morrisjs/morris.css"
	rel="stylesheet">
<!-- Custom Fonts -->
<link
	href="${pageContext.request.contextPath}/recourses/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<!-- ionicons-2.0.1 -->
<link
	href="${pageContext.request.contextPath}/recourses/vendor/ionicons-2.0.1/ionicons-2.0.1/css/ionicons.css">
<style>
@media ( min-width : 768px) {
}

@media ( min-width : 768px) {
	.p_body {
		margin-left: 10%;
	}
}

@media ( min-width : 1200px) {
	.p_body {
		margin-left: 26%;
	}
}

.page-wrapper {
	padding: 0 15px;
	height: 100%;
}

@media ( min-width : 768px) {
	.page-wrapper {
		position: inherit;
		margin: 0 0 0 250px;
		padding: 0 30px;
		border-left: 1px solid #e7e7e7;
	}
}
</style>
</head>
<script
	src="${pageContext.request.contextPath}/recourses/vendor/jquery/jquery.min.js"></script>
<script
	src="${pageContext.request.contextPath}/recourses/vendor/bootstrap/js/bootstrap.min.js"></script>
<script
	src="${pageContext.request.contextPath}/recourses/vendor/metisMenu/metisMenu.min.js"></script>
<script
	src="${pageContext.request.contextPath}/recourses/vendor/raphael/raphael.min.js"></script>
<script
	src="${pageContext.request.contextPath}/recourses/vendor/morrisjs/morris.min.js"></script>
<script
	src="${pageContext.request.contextPath}/recourses/data/morris-data.js"></script>
<script
	src="${pageContext.request.contextPath}/recourses/dist/js/sb-admin-2.js"></script>
<script
	src="${pageContext.request.contextPath}/recourses/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script
	src="${pageContext.request.contextPath}/recourses/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script
	src="${pageContext.request.contextPath}/recourses/vendor/datatables-responsive/dataTables.responsive.js"></script>
<body>

	<div id="wrapper">
		<img
			src="${pageContext.request.contextPath}/recourses/img/8C24D2C5F54426C7B09C0883F245397E.jpg"
			style="width: 100%">
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.html">学生管理系统</a>
			</div>
			<ul class="nav navbar-top-links navbar-right">
				<li>
					<p>欢迎,李安心！</p>
				</li>
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#"> <i class="fa fa-user fa-fw"></i>
						<i class="fa fa-caret-down"></i>
				</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="#"><i class="fa fa-user fa-fw "></i>修改密码</a></li>
						<li><a href="#"><i class="fa fa-user fa-fw"></i>个人信息</a></li>
						<li class="divider"></li>
						<li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i>
								退出</a></li>
					</ul></li>
			</ul>
			<div class="navbar-default sidebar" role="navigation">
				<div class="sidebar-nav navbar-collapse">
					<ul class="nav" id="side-menu">
						<li class="sidebar-search"></li>
						<c:forEach items="${modules }" var="module">
							<li><a
								href="${pageContext.request.contextPath}/${module.mUrl }"><i
									class="fa fa-align-justify"></i>${module.mName }</a></li>
						</c:forEach>
					</ul>
				</div>
			</div>
		</nav>
		<div id="page-wrapper">
			<c:if test="${not empty requestScope.goParam}">
				<jsp:include page="${requestScope.goParam }"></jsp:include>
			</c:if>
		</div>
	</div>
</body>
</html>
