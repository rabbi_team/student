<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="${pageContext.request.contextPath}/recourses/js/jsutil.js"></script>
<script src="${pageContext.request.contextPath}/recourses/js/infoSet.js"></script>
<div class="row">
	<!-- 添加模态框 -->
	<div class="col-lg-12">
		<h1 class="page-header">
			普通用户管理
			<button type="button" class="btn btn-outline btn-success"
				id="showModal">添加普通用户</button>
		</h1>
		<div style="margin-bottom: auto" class="modal fade" id="adduser"
			tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
			aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">添加普通用户</h4>
					</div>
					<div class="modal-body">
						<form id="addForm">
							<div class="form-group">
								<h4 class="modal-title" id="myModalLabel">用户名</h4>
								<input class="form-control" name="uUsername">
							</div>
							<div class="form-group">
								<h4 class="modal-title" id="myModalLabel">密码</h4>
								<input class="form-control" name="uPassword" type="password">
							</div>
							<div class="form-group">
								<h4 class="modal-title" id="myModalLabel">性别</h4>
								<div class="form-group">
									<select class="form-control" name="uGender" id="uGender">
										<option value="1">男</option>
										<option value="2">女</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<h4 class="modal-title" id="myModalLabel">姓名</h4>
								<input class="form-control" name="uName">
							</div>
							<div class="form-group">
								<h4 class="modal-title" id="myModalLabel">角色</h4>
								<div class="form-group">
									<select class="form-control" name="uRId" id="uRId">

									</select>
								</div>
							</div>
							<div class="form-group">
								<h4 class="modal-title" id="myModalLabel">可用状态</h4>
								<div class="form-group">
									<select class="form-control" name="uStatus" id="uStatus">
										<option value="1">可用</option>
										<option value="2">不可用</option>
									</select>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button type="button" id="addBtn" class="btn btn-primary">提交更改</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 修改模态框 -->
	<div style="margin-bottom: auto" class="modal fade" id="updUser"
		tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
		aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">修改普通用户</h4>
				</div>
				<div class="modal-body">
					<form id="updForm">
						<input type="hidden" name="uId" id="updateUId">
						<div class="form-group">
							<h4 class="modal-title" id="myModalLabel">用户名</h4>
							<input class="form-control" name="uUsername" id="updateUUsername">
						</div>
						<div class="form-group">
							<h4 class="modal-title" id="myModalLabel">密码</h4>
							<input class="form-control" type="password" name="uPassword"
								id="updateUPassword">
						</div>
						<div class="form-group">
							<h4 class="modal-title" id="myModalLabel">姓名</h4>
							<input class="form-control" name="uName" id="updateUName">
						</div>
						<div class="form-group">
							<h4 class="modal-title" id="myModalLabel">性别</h4>
							<div class="form-group">
								<select class="form-control" name="uGender" id="updateUGender">
									<option value="1">男</option>
									<option value="2">女</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<h4 class="modal-title" id="myModalLabel">角色</h4>
							<div class="form-group">
								<select class="form-control" name="uRId" id="role">
								</select>
							</div>
						</div>
						<div class="form-group">
							<h4 class="modal-title" id="myModalLabel">可用状态</h4>
							<div class="form-group">
								<select class="form-control" name="uStatus" id="updateUStatus">
									<option value="1">可用</option>
									<option value="2">不可用</option>
								</select>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" id="updBtn" class="btn btn-primary">提交更改</button>
				</div>
			</div>
		</div>
	</div>
	<!-- 数据列表 -->
	<table width="100%;"
		class="table table-striped table-bordered table-hover table-responsive text-center">
		<thead>
			<tr>
				<th>操作</th>
				<th>用户名</th>
				<th>姓名</th>
				<th>性别</th>
				<th>可用状态</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pageInfo.list }" var="user">
				<tr>
					<td><button type="button" class="btn btn-outline btn-info"
							onclick=<c:if test="${empty limitMap.update }">
									"error()"
									</c:if>
							<c:if test="${not empty limitMap.update }">
									"selectOne(${user.uId})"
									</c:if>>修改</button>
						<button type="button" class="btn btn-outline btn-danger"
							onclick=<c:if test="${empty limitMap.delete }">
									"error()"
									</c:if>
							<c:if test="${not empty limitMap.delete }">
									"deleteByUId(${user.uId})"
									</c:if>>删除</button>
					<td><p>${user.uUsername }</p></td>
					<td><p>${user.uName }</p></td>
					<td><p>${user.uGender eq '1'?'男':'女' }</p></td>
					<td><p>${user.uStatus eq '1'?'可用':'不可用' }</p></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<ul class="pagination">
		<c:if test="${pageInfo.hasPreviousPage}">
			<li><a
				href="${pageContext.request.contextPath}/User/selectAll?curPage=${pageInfo.prePage }">&laquo;</a></li>
		</c:if>
		<c:forEach items="${pageInfo.navigatepageNums }" var="pageNumber">
			<li
				<c:if  test="${pageNumber eq pageInfo.pageNum }">
							class="active"
						</c:if>><a
				<c:if test="${pageNumber != pageInfo.pageNum }">					
						href="${pageContext.request.contextPath}/User/selectAll?curPage=${pageNumber }"
						</c:if>>${pageNumber }</a></li>
		</c:forEach>
		<c:if test="${pageInfo.hasNextPage }">
			<li><a
				href="${pageContext.request.contextPath}/User/selectAll?curPage=${pageInfo.nextPage }">&raquo;</a></li>
		</c:if>
	</ul>
</div>
<script>
	//显示模态框
	$("#showModal").click(function(){
		if(${empty limitMap.insert }){
			error();
			return;
		}
		$.ajax({
			type : "post",
			url : "${pageContext.request.contextPath}/Rol/selectPass",
			data : {},
			success : function(data) {
				generateSelectByRol("uRId", data);
				$("#adduser").modal();
			}
		});
	});
	//增加功能
	$("#addBtn").click(function() {
		$.ajax({
			type : "post",
			url : "${pageContext.request.contextPath}/${empty limitMap.insert ? '':limitMap.insert.lUrl}",
			data : $("#addForm").serialize(),
			success : function(data) {
				if (data == 'success') {
					alert("添加成功");
					location.reload();
				} else
					alert("添加失败");
			}
		});
	});
	//删除功能
	function deleteByUId(uidVar) {
		$.ajax({
			type : "post",
			url : "${pageContext.request.contextPath}/${empty limitMap.delete ? '':limitMap.delete.lUrl}",
			data : {
				uId : uidVar
			},
			success : function(data) {
				if (data == 'success') {
					alert("删除成功");
					location.reload();
				} else
					alert("删除失败");
			}
		});
	}
	//修改功能
	$("#updBtn").click(function() {
		$.ajax({
			type : "post",
			url : "${pageContext.request.contextPath}/${empty limitMap.update ? '':limitMap.update.lUrl}",
			data : $("#updForm").serialize(),
			success : function(data) {
				if (data == 'success') {
					alert("修改成功");
					location.reload();
				} else
					alert("修改失败");
			}
		});
	});
	//查询单个
	function selectOne(uIdVar) {
		$.ajax({
			type : "post",
			url : "${pageContext.request.contextPath}/Rol/selectPass",
			data : {},
			success : function(data) {
				generateSelectByRol("role", data);
			}
		});
		$.ajax({
			type : "post",
			url : "${pageContext.request.contextPath}/User/selectOne",
			data : {
				uId : uIdVar
			},
			success : function(data) {
				for (curStrKey in data) {
					if (curStrKey == 'uRId' || curStrKey == 'uStatus'
							|| curStrKey == 'uGender')
						continue;
					$("#update" + title(curStrKey)).val(data[curStrKey]);
				}
				setOptionSelectedByValue("updateUGender", data.uGender);
				setOptionSelectedByValue("role", data.uRId);
				setOptionSelectedByValue("updateUStatus", data.uStatus);
				$("#updUser").modal();
			}
		});
	}
	function error(){
		alert("你所属用户的权限无法使用该功能");
	}
</script>