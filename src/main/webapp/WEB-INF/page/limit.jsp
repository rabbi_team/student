<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="${pageContext.request.contextPath}/recourses/js/jsutil.js"></script>
<script src="${pageContext.request.contextPath}/recourses/js/infoSet.js"></script>
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">
			权限管理
			<button type="button" class="btn btn-outline btn-success"
				id="showAddModual">添加权限</button>
		</h1>
		<div style="margin-bottom: auto" class="modal fade" id="addModal"
			tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
			aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">修改</h4>
					</div>
					<div class="modal-body">
						<form id="addForm">
							<div class="form-group">
								<h4 class="modal-title" id="myModalLabel">权限名称</h4>
								<input class="form-control" name="lName">
							</div>
							<div class="form-group">
								<h4 class="modal-title" id="myModalLabel">权限路径</h4>
								<input class="form-control" name="lUrl">
							</div>
							<div class="form-group">
								<h4 class="modal-title" id="myModalLabel">所属模块</h4>
								<select id="addModual" name="lMId" class="form-control">

								</select>
							</div>
							<div class="form-group">
								<h4 class="modal-title" id="myModalLabel">可用状态</h4>
								<div class="form-group">
									<select class="form-control" name="lStatus">
										<option value="1">可用</option>
										<option value="2">不可用</option>
									</select>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button type="button" class="btn btn-primary" id="addBtn">提交更改</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$("#showAddModual").click(function() {
		$.ajax({
			type : "post",
			url : "${pageContext.request.contextPath}/Module/selectAll",
			data : {
				status:"1"
			},
			success : function(data) {
				generateSelecctByLimit("addModual", data);
				$("#addModal").modal();
			}
		});
	});
	$("#addBtn").click(function() {
		$.ajax({
			type : "post",
			url : "${pageContext.request.contextPath}/Limit/insert",
			data : $("#addForm").serialize(),
			success : function(data) {
				if (data == "success") {
					alert("添加成功");
					location.reload();
				} else
					alert("添加失败");
			}
		});
	});
</script>
<div style="margin-bottom: auto" class="modal fade" id="updModal"
	tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">修改</h4>
			</div>
			<div class="modal-body">
				<form id="updForm">
					<input type="hidden" class="form-control" id="updLId" name="lId">
					<div class="form-group">
						<h4 class="modal-title" id="myModalLabel">权限名称</h4>
						<input class="form-control" id="updLName" name="lName">
					</div>
					<div class="form-group">
						<h4 class="modal-title" id="myModalLabel">权限路径</h4>
						<input class="form-control" id="updLUrl" name="lUrl">
					</div>
					<div class="form-group">
						<h4 class="modal-title" id="myModalLabel">所属模块</h4>
						<select id="updModual" name="lMId" class="form-control">

						</select>
					</div>
					<div class="form-group">
						<h4 class="modal-title" id="myModalLabel">可用状态</h4>
						<div class="form-group">
							<select class="form-control" name="lStatus" id="status">
								<option value="1">可用</option>
								<option value="2">不可用</option>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="updBtn">提交更改</button>
			</div>
		</div>
	</div>
</div>
<script>
	$("#updBtn").click(function() {
		$.ajax({
			type : "post",
			url : "${pageContext.request.contextPath}/Limit/update",
			data : $("#updForm").serialize(),
			success : function(data) {
				if (data == "success") {
					alert("修改成功");
					location.reload();
				} else
					alert("修改失败");
			}
		});
	});
</script>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-body">
			<table width="100%;"
				class="table table-striped table-bordered table-hover table-responsive text-center">
				<thead>
					<tr>
						<th>操作</th>
						<th>权限名称</th>
						<th>权限路径</th>
						<th>可用状态</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pageInfo.list }" var="limit">
						<tr>
							<td><button type="button" class="btn btn-outline btn-info"
									onclick="selectOne(${limit.lId })">修改</button>
								<button type="button" class="btn btn-outline btn-danger"
									onclick="deleteOne(${limit.lId })">删除</button></td>
							<td><p>${limit.lName }</p></td>
							<td><p>${limit.lUrl }</p></td>
							<td><p>${limit.lStatus eq '1'?'可用':'不可用' }</p></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<ul class="pagination">
				<c:if test="${pageInfo.hasPreviousPage}">
					<li><a
						href="${pageContext.request.contextPath}/Limit/selectAll?curPage=${pageInfo.prePage }">&laquo;</a></li>
				</c:if>
				<c:forEach items="${pageInfo.navigatepageNums }" var="pageNumber">
					<li
						<c:if  test="${pageNumber eq pageInfo.pageNum }">
							class="active"
						</c:if>><a
						<c:if test="${pageNumber != pageInfo.pageNum }">					
						href="${pageContext.request.contextPath}/Limit/selectAll?curPage=${pageNumber }"
						</c:if>>${pageNumber }</a></li>
				</c:forEach>
				<c:if test="${pageInfo.hasNextPage }">
					<li><a
						href="${pageContext.request.contextPath}/Limit/selectAll?curPage=${pageInfo.nextPage }">&raquo;</a></li>
				</c:if>
			</ul>
		</div>
	</div>
</div>
<script>
	function selectOne(lIdVar) {
		$.ajax({
			type : "post",
			url : "${pageContext.request.contextPath}/Module/selectAll",
			data : {status:'1'},
			success : function(data) {
				generateSelecctByLimit("updModual", data);
			}
		});
		$.ajax({
			type : "post",
			url : "${pageContext.request.contextPath}/Limit/selectOne",
			data : {
				lId : lIdVar
			},
			success : function(data) {
				for (curKey in data) {
					if (curKey == 'lMId' || curKey == 'lStatus')
						continue;
					$("#upd" + title(curKey)).val(data[curKey]);
				}
				setOptionSelectedByValue("updModual", data.lMId);
				setOptionSelectedByValue("status", data.lStatus);
				$("#updModal").modal();
			}
		});
	}
	function deleteOne(lIdVar) {
		$.ajax({
			type : "post",
			url : "${pageContext.request.contextPath}/Limit/delete",
			data : {
				lId : lIdVar
			},
			success : function(data) {
				if (data == "success") {
					alert("删除成功");
					location.reload();
				} else
					alert("删除失败");
			}
		});
	}
</script>