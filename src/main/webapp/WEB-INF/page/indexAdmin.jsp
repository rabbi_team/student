<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="${pageContext.request.contextPath}/recourses/js/jsutil.js"></script>
<script src="${pageContext.request.contextPath}/recourses/js/infoSet.js"></script>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">
			模块管理
			<button type="button" class="btn btn-outline btn-success"
				id="showModal">添加模块</button>
		</h1>
		<div style="margin-bottom: auto" class="modal fade" id="addModual"
			tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
			aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">添加模块户</h4>
					</div>
					<div class="modal-body">
						<form id="addForm">
							<div class="form-group">
								<h4 class="modal-title" id="myModalLabel">模块名称</h4>
								<input class="form-control" name="mName">
							</div>
							<div class="form-group">
								<h4 class="modal-title" id="myModalLabel">模块路径</h4>
								<input class="form-control" name="mUrl">
							</div>
							<div class="form-group">
								<h4 class="modal-title" id="myModalLabel">可用状态</h4>
								<div class="form-group">
									<select class="form-control" name="mStatus">
										<option value="1">可用</option>
										<option value="2">不可用</option>
									</select>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button type="button" id="addBtn" class="btn btn-primary">提交更改</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$("#addBtn")
			.click(
					function() {
						$
								.ajax({
									type : "post",
									url : "${pageContext.request.contextPath}/${empty limitMap.insert ? '':limitMap.insert.lUrl}",
									data : $("#addForm").serialize(),
									success : function(data) {
										if (data == "success") {
											alert("添加成功");
											location.reload();
										} else
											alert("添加失败");
									}
								});
					});
	$("#showModal").click(function() {
		<c:if test="${empty limitMap.insert }">
		alert("你所属的用户没有权限使用该功能");
		</c:if>
		<c:if test="${not empty limitMap.insert }">
		$("#addModual").modal();
		</c:if>
	});
</script>
<div style="margin-bottom: auto" class="modal fade" id="updModal"
	tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">修改</h4>
			</div>
			<div class="modal-body">
				<form id="updForm">
					<input type="hidden" name="mId" id="updMId">
					<div class="form-group">
						<h4 class="modal-title" id="myModalLabel">模块名称</h4>
						<input class="form-control" name="mName" id="updMName">
					</div>
					<div class="form-group">
						<h4 class="modal-title" id="myModalLabel">模块路径</h4>
						<input class="form-control" name="mUrl" id="updMUrl">
					</div>
					<div class="form-group">
						<h4 class="modal-title" id="myModalLabel">可用状态</h4>
						<div class="form-group">
							<select class="form-control" name="mStatus" id="updMStatus">
								<option value="1">可用</option>
								<option value="2">不可用</option>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" id="updBtn" class="btn btn-primary">提交更改</button>
			</div>
		</div>
	</div>
</div>
<script>
	$("#updBtn")
			.click(
					function() {
						$
								.ajax({
									type : "post",
									url : "${pageContext.request.contextPath}/${empty limitMap.update ? '':limitMap.update.lUrl}",
									data : $("#updForm").serialize(),
									success : function(data) {
										if (data == "isUse")
											alert("该模块已经有用户正在使用,无法修改");
										else if (data == "error")
											alert("修改失败");
										else {
											alert("修改成功");
											location.reload();
										}
									}
								});
					});
</script>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-body">
			<table width="100%;"
				class="table table-striped table-bordered table-hover table-responsive text-center">
				<thead>
					<tr>
						<th>操作</th>
						<th>模块名</th>
						<th>模块路径</th>
						<th>可用状态</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pageInfo.list }" var="moduale">
						<tr>
							<td><button type="button" class="btn btn-outline btn-info"
									<c:if test="${empty limitMap.update }">
										onclick="error()"
									</c:if>
									<c:if test="${not empty limitMap.update }">
										onclick="selectOne(${moduale.mId})"
									</c:if>>修改</button>
								<button type="button" class="btn btn-outline btn-danger"
									<c:if test="${empty limitMap.delete }">
										onclick="error()"
									</c:if>
									<c:if test="${not empty limitMap.delete }">
										onclick="deleteOne(${moduale.mId})"
									</c:if>>删除</button></td>
							<td><p>${moduale.mName }</p></td>
							<td><p>${moduale.mUrl }</p></td>
							<td><p>${moduale.mStatus eq '1'?'可用':'不可用' }</p></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<ul class="pagination">
				<c:if test="${pageInfo.hasPreviousPage}">
					<li><a
						href="${pageContext.request.contextPath}/Module/selectAllindex?curPage=${pageInfo.prePage }">&laquo;</a></li>
				</c:if>
				<c:forEach items="${pageInfo.navigatepageNums }" var="pageNumber">
					<li
						<c:if  test="${pageNumber eq pageInfo.pageNum }">
							class="active"
						</c:if>><a
						<c:if test="${pageNumber != pageInfo.pageNum }">					
						href="${pageContext.request.contextPath}/Module/selectAllindex?curPage=${pageNumber }"
						</c:if>>${pageNumber }</a></li>
				</c:forEach>
				<c:if test="${pageInfo.hasNextPage }">
					<li><a
						href="${pageContext.request.contextPath}/Module/selectAllindex?curPage=${pageInfo.nextPage }">&raquo;</a></li>
				</c:if>
			</ul>
		</div>
	</div>
</div>
<script>
	function selectOne(mIdVar) {
		$.ajax({
			type : "post",
			url : "${pageContext.request.contextPath}/Module/selectOne",
			data : {
				mId : mIdVar
			},
			success : function(data) {
				for (curKey in data) {
					if (curKey == "mStatus")
						continue;
					$("#upd" + title(curKey)).val(data[curKey]);
				}
				setOptionSelectedByValue("updMStatus", data.mStatus);
				$("#updModal").modal();
			}
		});
	}
	function deleteOne(mIdVar) {
		$
				.ajax({
					type : "post",
					url : "${pageContext.request.contextPath}/${empty limitMap.delete ? '':limitMap.delete.lUrl}",
					data : {
						mId : mIdVar
					},
					success : function(data) {
						if (data == "isUse")
							alert("该模块已经有用户正在使用,无法删除");
						else if (data == "error")
							alert("删除失败");
						else {
							alert("删除成功");
							location.reload();
						}
					}
				});
	}
	function error() {
		alert("你所属用户的权限不能使用该功能");
	}
</script>