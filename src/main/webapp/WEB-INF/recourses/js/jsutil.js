/**
 * 判断字符串是否存在自一个字符串数组中
 * 
 * @param arrVar
 *            字符串数组
 * @param cmpVar
 *            进行判断的子字符串值
 * @returns true 存在 false不存在
 */
function isHaveByArr(arrVar, cmpVar) {

	for (var i = 0; i < arrVar.length; i++) {
		if (arrVar[i] == cmpVar)
			return true;
	}
	return false;
}
/**
 * 
 * 清除一个容器下的所有input的checked选中状态和value值
 * 
 * @param fatherElement
 *            想要清除的input的value或者checked选中状态的父容器ID
 */
function clearInpiut(fatherElement) {
	$("#" + fatherElement).find("input").each(
			function() {
				if ($(this).prop('type') == 'radio'
						|| $(this).prop('type') == 'checkbox') {
					$(this).prop('checked', false);
				} else {
					$(this).val('');
				}
			});
}
/**
 * 
 * @param selectParent
 * @returns
 */
function getOptionValueBySelected(selectParent) {
	var selectVar = $("#" + selectParent);
	var valueVar = '';
	selectVar.find("option").each(function() {
		if ($(this).prop('selected')) {
			valueVar = $(this).val();
			return false;
		}
	});
	return valueVar;
}
/**
 * 
 * @param id
 * @returns
 */
function setOptionSelectedByValue(selectParent, value) {
	var selectVar = $("#" + selectParent);
	selectVar.find("option").each(function() {
		if ($(this).val() == value) {
			$(this).prop("selected", true);
			return;
		}
	});
}

function clearByName(tpye, name) {
	$(tpye + "name[" + name + "]").remove();
}

/**
 * 首字母开头大写
 * 
 * @param strVar
 * @returns
 */
function title(strVar) {
	return strVar.substring(0, 1).toUpperCase() + strVar.substring(1);
}