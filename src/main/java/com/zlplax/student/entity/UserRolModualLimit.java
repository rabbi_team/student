package com.zlplax.student.entity;

import java.io.Serializable;


@SuppressWarnings("serial")
public class UserRolModualLimit implements Serializable {

	private int urmlId;
	private int urmlUId;
	private int urmlRId;
	private int urmlMId;
	private int urmlLId;
	private String urmlStatus;
	public UserRolModualLimit() {
		super();
	}

	public UserRolModualLimit (int urmlId,int urmlUId,int urmlRId,int urmlMId,int urmlLId,String urmlStatus) {
		super();
		this.urmlId = urmlId;
		this.urmlUId = urmlUId;
		this.urmlRId = urmlRId;
		this.urmlMId = urmlMId;
		this.urmlLId = urmlLId;
		this.urmlStatus = urmlStatus;
	}

	public void seturmlId(int urmlId) {
		this.urmlId = urmlId;
	}

	public int geturmlId() {
		return this.urmlId;
	}

	public void seturmlUId(int urmlUId) {
		this.urmlUId = urmlUId;
	}

	public int geturmlUId() {
		return this.urmlUId;
	}

	public void seturmlRId(int urmlRId) {
		this.urmlRId = urmlRId;
	}

	public int geturmlRId() {
		return this.urmlRId;
	}

	public void seturmlMId(int urmlMId) {
		this.urmlMId = urmlMId;
	}

	public int geturmlMId() {
		return this.urmlMId;
	}

	public void seturmlLId(int urmlLId) {
		this.urmlLId = urmlLId;
	}

	public int geturmlLId() {
		return this.urmlLId;
	}

	public void seturmlStatus(String urmlStatus) {
		this.urmlStatus = urmlStatus;
	}

	public String geturmlStatus() {
		return this.urmlStatus;
	}

	public String toString() {
		return "UserRolModualLimit ["+"urmlId:"+this.urmlId+","+"urmlUId:"+this.urmlUId+","+"urmlRId:"+this.urmlRId+","+"urmlMId:"+this.urmlMId+","+"urmlLId:"+this.urmlLId+","+"urmlStatus:"+this.urmlStatus+"]";
	}
}
