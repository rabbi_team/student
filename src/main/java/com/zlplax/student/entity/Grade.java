package com.zlplax.student.entity;

import java.io.Serializable;


@SuppressWarnings("serial")
public class Grade implements Serializable {

	private int gId;
	private int gUId;
	private float gValue;
	public Grade() {
		super();
	}

	public Grade (int gId,int gUId,float gValue) {
		super();
		this.gId = gId;
		this.gUId = gUId;
		this.gValue = gValue;
	}

	public void setgId(int gId) {
		this.gId = gId;
	}

	public int getgId() {
		return this.gId;
	}

	public void setgUId(int gUId) {
		this.gUId = gUId;
	}

	public int getgUId() {
		return this.gUId;
	}

	public void setgValue(float gValue) {
		this.gValue = gValue;
	}

	public float getgValue() {
		return this.gValue;
	}

	public String toString() {
		return "Grade ["+"gId:"+this.gId+","+"gUId:"+this.gUId+","+"gValue:"+this.gValue+"]";
	}
}
