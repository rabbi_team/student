package com.zlplax.student.entity;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Limit implements Serializable {

	private int lId;
	private String lName;
	private int lMId;
	private String lUrl;
	private String lStatus;

	public Limit() {
		super();
	}

	public Limit(int lId, String lName, int lMId, String lUrl, String lStatus) {
		super();
		this.lId = lId;
		this.lName = lName;
		this.lMId = lMId;
		this.lUrl = lUrl;
		this.lStatus = lStatus;
	}

	public void setlId(int lId) {
		this.lId = lId;
	}

	public int getlId() {
		return this.lId;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getlName() {
		return this.lName;
	}

	public void setlMId(int lMId) {
		this.lMId = lMId;
	}

	public int getlMId() {
		return this.lMId;
	}

	public void setlUrl(String lUrl) {
		this.lUrl = lUrl;
	}

	public String getlUrl() {
		return this.lUrl;
	}

	public void setlStatus(String lStatus) {
		this.lStatus = lStatus;
	}

	public String getlStatus() {
		return this.lStatus;
	}

	public String toString() {
		return "Limit [" + "lId:" + this.lId + "," + "lName:" + this.lName + "," + "lMId:" + this.lMId + "," + "lUrl:"
				+ this.lUrl + "," + "lStatus:" + this.lStatus + "]";
	}
}
