package com.zlplax.student.entity;

import java.io.Serializable;


@SuppressWarnings("serial")
public class Rol implements Serializable {

	private int rId;
	private String rName;
	private String rStatus;
	public Rol() {
		super();
	}

	public Rol (int rId,String rName,String rStatus) {
		super();
		this.rId = rId;
		this.rName = rName;
		this.rStatus = rStatus;
	}

	public void setrId(int rId) {
		this.rId = rId;
	}

	public int getrId() {
		return this.rId;
	}

	public void setrName(String rName) {
		this.rName = rName;
	}

	public String getrName() {
		return this.rName;
	}

	public void setrStatus(String rStatus) {
		this.rStatus = rStatus;
	}

	public String getrStatus() {
		return this.rStatus;
	}

	public String toString() {
		return "Rol ["+"rId:"+this.rId+","+"rName:"+this.rName+","+"rStatus:"+this.rStatus+"]";
	}
}
