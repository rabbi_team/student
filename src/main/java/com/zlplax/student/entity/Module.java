package com.zlplax.student.entity;

import java.io.Serializable;


@SuppressWarnings("serial")
public class Module implements Serializable {

	private int mId;
	private String mName;
	private String mUrl;
	private String mStatus;
	public Module() {
		super();
	}

	public Module (int mId,String mName,String mUrl,String mStatus) {
		super();
		this.mId = mId;
		this.mName = mName;
		this.mUrl = mUrl;
		this.mStatus = mStatus;
	}

	public void setmId(int mId) {
		this.mId = mId;
	}

	public int getmId() {
		return this.mId;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getmName() {
		return this.mName;
	}

	public void setmUrl(String mUrl) {
		this.mUrl = mUrl;
	}

	public String getmUrl() {
		return this.mUrl;
	}

	public void setmStatus(String mStatus) {
		this.mStatus = mStatus;
	}

	public String getmStatus() {
		return this.mStatus;
	}

	public String toString() {
		return "Module ["+"mId:"+this.mId+","+"mName:"+this.mName+","+"mUrl:"+this.mUrl+","+"mStatus:"+this.mStatus+"]";
	}
}
