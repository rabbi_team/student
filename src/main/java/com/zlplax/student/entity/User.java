package com.zlplax.student.entity;

import java.io.Serializable;

@SuppressWarnings("serial")
public class User implements Serializable {

	private int uId;
	
	private String uUsername;
	
	private String uPassword;
	
	private String uName;
	
	private String uGender;
	
	private int uRId;
	
	private String uStatus;

	public User() {
		super();
	}

	public User(int uId, String uUsername, String uPassword, String uName, String uGender, int uRId, String uStatus) {
		super();
		this.uId = uId;
		this.uUsername = uUsername;
		this.uPassword = uPassword;
		this.uName = uName;
		this.uGender = uGender;
		this.uRId = uRId;
		this.uStatus = uStatus;
	}

	public void setuId(int uId) {
		this.uId = uId;
	}

	public int getuId() {
		return this.uId;
	}

	public void setuUsername(String uUsername) {
		this.uUsername = uUsername;
	}

	public String getuUsername() {
		return this.uUsername;
	}

	public void setuPassword(String uPassword) {
		this.uPassword = uPassword;
	}

	public String getuPassword() {
		return this.uPassword;
	}

	public void setuName(String uName) {
		this.uName = uName;
	}

	public String getuName() {
		return this.uName;
	}

	public void setuGender(String uGender) {
		this.uGender = uGender;
	}

	public String getuGender() {
		return this.uGender;
	}

	public void setuRId(int uRId) {
		this.uRId = uRId;
	}

	public int getuRId() {
		return this.uRId;
	}

	public void setuStatus(String uStatus) {
		this.uStatus = uStatus;
	}

	public String getuStatus() {
		return this.uStatus;
	}

	public String toString() {
		return "User [" + "uId:" + this.uId + "," + "uUsername:" + this.uUsername + "," + "uPassword:" + this.uPassword
				+ "," + "uName:" + this.uName + "," + "uGender:" + this.uGender + "," + "uRId:" + this.uRId + ","
				+ "uStatus:" + this.uStatus + "]";
	}
}
