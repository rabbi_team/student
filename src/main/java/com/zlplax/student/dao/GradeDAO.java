package com.zlplax.student.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.zlplax.student.entity.Grade;

@Repository
public interface GradeDAO{
	public int insertByGrade(@Param("grade") Grade grade);

	public int deleteById(@Param("gId") int gId);

	public int updateByGrade(@Param("grade") Grade grade);

	public Grade selectOne(@Param("gId") int gId);

	public List<Grade> selectAll();

}
