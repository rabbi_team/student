package com.zlplax.student.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.zlplax.student.entity.Module;

@Repository
public interface ModuleDAO {
	public int insertByModule(@Param("module") Module module);

	public int deleteById(@Param("mId") int mId);

	public int updateByModule(@Param("module") Module module);

	public Module selectOne(@Param("mId") int mId);

	public int selectByPath(@Param("pathUrl") String pathUrl);

	public List<Module> selectAllByUIdArray(@Param("intArr") List<Integer> intArr);

	public List<Module> selectAllByStatus(@Param("mStatus") String mStatus);

	public List<Module> selectAll();

}
