package com.zlplax.student.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.zlplax.student.entity.UserRolModualLimit;

@Repository
public interface UserRolModualLimitDAO {
	public int insertByUserRolModualLimit(@Param("userRolModualLimit") UserRolModualLimit userRolModualLimit);

	public int deleteById(@Param("urmlId") int urmlId);

	public int updateByUserRolModualLimit(@Param("userRolModualLimit") UserRolModualLimit userRolModualLimit);

	public UserRolModualLimit selectOne(@Param("urmlId") int urmlId);

	public int selectForeign(@Param("uId") int uId, @Param("rId") int rId, @Param("mId") int mId,
			@Param("lId") int lId);

	public List<Integer> selectMIdByStatusAndUid(@Param("status") String status, @Param("uId") int uId);

	public List<UserRolModualLimit> selectAll();

}
