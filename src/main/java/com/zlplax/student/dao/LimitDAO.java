package com.zlplax.student.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.zlplax.student.entity.Limit;

@Repository
public interface LimitDAO {
	public int insertByLimit(@Param("limit") Limit limit);

	public int deleteById(@Param("lId") int lId);

	public int updateByLimit(@Param("limit") Limit limit);

	public Limit selectOne(@Param("lId") int lId);

	public Limit selectlIdByMIdAndType(@Param("mId") int mId, @Param("type") String type,
			@Param("status") String status);

	public List<Limit> selectAllByStatus(@Param("lStatus") String lStatus);

	public List<Limit> selectAll();

}
