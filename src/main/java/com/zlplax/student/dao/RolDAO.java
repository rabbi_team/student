package com.zlplax.student.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.zlplax.student.entity.Rol;

@Repository
public interface RolDAO {
	public int insertByRol(@Param("rol") Rol rol);

	public int deleteById(@Param("rId") int rId);

	public int updateByRol(@Param("rol") Rol rol);

	public Rol selectOne(@Param("rId") int rId);

	public List<Rol> selectAllByRStatus(@Param("rStatus") String rStatus);

	public List<Rol> selectAll();

}
