package com.zlplax.student.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.zlplax.student.entity.User;

@Repository
public interface UserDAO {
	public int insertByUser(@Param("user") User user);

	public int deleteById(@Param("uId") int uId);

	public int updateByUser(@Param("user") User user);

	public int selectCountByURId(@Param("uRId") int rId);

	public User selectOne(@Param("uId") int uId);

	public User selectUUsername(@Param("uUsername") String uUsername);

	public List<User> selectAllByStatus(@Param("uStatus") String uStatus);

	public List<User> selectAll();

}
