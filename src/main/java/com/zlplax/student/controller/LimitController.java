package com.zlplax.student.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zlplax.student.entity.Limit;
import com.zlplax.student.entity.Module;
import com.zlplax.student.entity.User;
import com.zlplax.student.service.LimitService;
import com.zlplax.student.service.ModuleService;
import com.zlplax.student.service.UserRolModualLimitService;

@Controller
@RequestMapping("Limit")
public class LimitController {
	@Autowired
	LimitService limitService;
	@Autowired
	ModuleService moduleSerive;
	@Autowired
	UserRolModualLimitService userRolModualLimitService;

	@RequestMapping("insert")
	@ResponseBody
	public String insert(Limit limit) {
		if (limitService.insertByLimit(limit) != 0)
			return "success";
		else
			return "error";
	}

	@RequestMapping("delete")
	@ResponseBody
	public String deleteOne(int lId) {
		if (limitService.deleteById(lId) != 0)
			return "success";
		else
			return "error";
	}

	@RequestMapping("update")
	@ResponseBody
	public String update(Limit limit) {
		if (limitService.updateByLimit(limit) != 0)
			return "success";
		else
			return "error";
	}

	@RequestMapping("selectOne")
	@ResponseBody
	public Limit selectOne(int lId) {
		return limitService.selectOne(lId);
	}

	@RequestMapping("selectAll")
	public String selectAll(HttpServletRequest request, HttpSession session) {
		User user = (User) session.getAttribute("user");
		request.setAttribute("modules", moduleSerive
				.selectAllByUIdArray(userRolModualLimitService.selectMIdByStatusAndUid("1", user.getuId())));
		boolean flag = false;
		String rStatus = "";
		if ((rStatus = request.getParameter("isStatus")) != null)
			flag = true;
		String pageStr = request.getParameter("curPage");
		int curPage = pageStr == null ? 1 : !pageStr.matches("\\d+") ? 1 : Integer.parseInt(pageStr);
		PageHelper.startPage(curPage, 5);
		PageHelper.orderBy("l_id desc");
		PageInfo<Limit> pageInfo = new PageInfo<>(
				flag ? limitService.selectAllByStatus(rStatus) : limitService.selectAll());
		request.setAttribute("pageInfo", pageInfo);
		request.setAttribute("goParam", "limit.jsp");
		return "indexSuperadmin";
	}
}
