package com.zlplax.student.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;

import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zlplax.student.entity.Module;
import com.zlplax.student.entity.User;
import com.zlplax.student.service.ModuleService;
import com.zlplax.student.service.UserRolModualLimitService;

@Controller
@RequestMapping("/")
public class GoPageController {

	@Autowired
	ModuleService moduleService;
	@Autowired
	UserRolModualLimitService userRolModualLimitService;

	@RequestMapping("login")
	public String goLogin() {
		return "Login";
	}

	@RequestMapping("indexSuperadmin")
	public String goIndexSuperadmin(HttpServletRequest request, HttpSession session) {
		User user = (User) session.getAttribute("user");
		List<Integer> list = userRolModualLimitService.selectMIdByStatusAndUid("1", user.getuId());
		if (list.size() == 0)
			list = null;
		request.setAttribute("modules", moduleService.selectAllByUIdArray(list));
		return "indexSuperadmin";
	}
}
