package com.zlplax.student.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zlplax.student.entity.Limit;
import com.zlplax.student.entity.Rol;
import com.zlplax.student.entity.User;
import com.zlplax.student.service.LimitService;
import com.zlplax.student.service.ModuleService;
import com.zlplax.student.service.RolService;
import com.zlplax.student.service.UserRolModualLimitService;
import com.zlplax.student.service.UserService;

@Controller
@RequestMapping("Rol")
public class RolController {
	@Autowired
	RolService rolService;
	@Autowired
	UserService userService;
	@Autowired
	ModuleService moduleService;
	@Autowired
	UserRolModualLimitService userRolModualLimitService;
	@Autowired
	LimitService limitService;
	@RequestMapping("selectPass")
	@ResponseBody
	public List<Rol> selectPass() {
		return rolService.selectAllByRStatus("1");
	}

	@RequestMapping("insert")
	@ResponseBody
	public String insertByRol(Rol rol) {
		if (rolService.insertByRol(rol) != 0)
			return "success";
		else
			return "error";
	}

	@RequestMapping("delete")
	@ResponseBody
	public String delete(int rId) {
		if (userService.selectCountByURId(rId) != 0)
			return "isUse";
		System.out.println(rId);
		if (rolService.deleteById(rId) != 0)
			return "success";
		else
			return "error";
	}

	@RequestMapping("update")
	@ResponseBody
	public String update(Rol rol) {
		if (userService.selectCountByURId(rol.getrId()) != 0)
			return "isUse";
		if (rolService.updateByRol(rol) != 0)
			return "success";
		else
			return "error";
	}

	@RequestMapping("selectOne")
	@ResponseBody
	public Rol selectOne(int rId) {
		return rolService.selectOne(rId);
	}

	@RequestMapping("selectAll")
	public String selectAll(HttpServletRequest request, HttpSession session) {
		User user = (User) session.getAttribute("user");
		List<Integer> list = userRolModualLimitService.selectMIdByStatusAndUid("1", user.getuId());
		if (list.size() == 0)
			list = null;
		request.setAttribute("modules", moduleService
				.selectAllByUIdArray(list));
		boolean flag = false;
		String rStatus = "";
		if ((rStatus = request.getParameter("isStatus")) != null)
			flag = true;
		String pageStr = request.getParameter("curPage");
		int curPage = pageStr == null ? 1 : !pageStr.matches("\\d+") ? 1 : Integer.parseInt(pageStr);
		PageHelper.startPage(curPage, 5);
		PageHelper.orderBy("r_id desc");
		PageInfo<Rol> pageInfo = new PageInfo<>(flag ? rolService.selectAllByRStatus(rStatus) : rolService.selectAll());
		Limit limitInsert = null;
		Limit limitDelete = null;
		Limit limitUpdate = null;
		int mId = moduleService.selectByPath("Rol/selectAll");
		int uId = user.getuId();
		int rId = user.getuRId();
		int insertlId = (limitInsert = limitService.selectlIdByMIdAndType(mId, "����", "1")) == null ? 0
				: limitInsert.getlId();
		int deletelId = (limitDelete = limitService.selectlIdByMIdAndType(mId, "ɾ��", "1")) == null ? 0
				: limitDelete.getlId();
		int updatelId = (limitUpdate = limitService.selectlIdByMIdAndType(mId, "�޸�", "1")) == null ? 0
				: limitUpdate.getlId();
		HashMap<String, Object> hashMap = new HashMap<>();
		if (userRolModualLimitService.selectForeign(uId, rId, mId, insertlId) != 0)
			hashMap.put("insert", limitInsert);
		if (userRolModualLimitService.selectForeign(uId, rId, mId, deletelId) != 0)
			hashMap.put("delete", limitDelete);
		if (userRolModualLimitService.selectForeign(uId, rId, mId, updatelId) != 0)
			hashMap.put("update", limitUpdate);
		request.setAttribute("limitMap", hashMap);
		request.setAttribute("pageInfo", pageInfo);
		request.setAttribute("goParam", "rol.jsp");
		return "indexSuperadmin";
	}
}
