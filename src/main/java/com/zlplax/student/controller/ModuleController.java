package com.zlplax.student.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zlplax.student.entity.Limit;
import com.zlplax.student.entity.Module;
import com.zlplax.student.entity.User;
import com.zlplax.student.service.LimitService;
import com.zlplax.student.service.ModuleService;
import com.zlplax.student.service.UserRolModualLimitService;

@Controller
@RequestMapping("Module")
public class ModuleController {
	@Autowired
	ModuleService moduleService;
	@Autowired
	UserRolModualLimitService userRolModualLimitService;
	@Autowired
	LimitService limitService;

	@RequestMapping("selectAll")
	@ResponseBody
	public List<Module> selectAll(String status) {
		return moduleService.selectAllByStatus(status);
	}

	@RequestMapping("insert")
	@ResponseBody
	public String insert(Module module) {
		if (moduleService.insertByModule(module) != 0)
			return "success";
		else
			return "error";
	}

	@RequestMapping("delete")
	@ResponseBody
	public String delete(int mId) {
		try {
			if (moduleService.deleteById(mId) != 0)
				return "success";
			else
				return "error";
		} catch (Exception e) {
			return "isUse";
		}
	}

	@RequestMapping("update")
	@ResponseBody
	public String update(Module module) {
		if (moduleService.updateByModule(module) != 0)
			return "success";
		else
			return "error";
	}

	@RequestMapping("selectOne")
	@ResponseBody
	public Module selectOne(int mId) {
		return moduleService.selectOne(mId);
	}

	@RequestMapping("selectAllindex")
	public String selectAll(HttpServletRequest request, HttpSession session) {
		User user = (User) session.getAttribute("user");
		List<Integer> list = userRolModualLimitService.selectMIdByStatusAndUid("1", user.getuId());
		if (list.size() == 0)
			list = null;
		request.setAttribute("modules", moduleService.selectAllByUIdArray(list));
		boolean flag = false;
		String rStatus = "";
		if ((rStatus = request.getParameter("isStatus")) != null)
			flag = true;
		String pageStr = request.getParameter("curPage");
		int curPage = pageStr == null ? 1 : !pageStr.matches("\\d+") ? 1 : Integer.parseInt(pageStr);
		PageHelper.startPage(curPage, 5);
		PageHelper.orderBy("m_id desc");
		PageInfo<Module> pageInfo = new PageInfo<>(
				flag ? moduleService.selectAllByStatus(rStatus) : moduleService.selectAll());
		request.setAttribute("pageInfo", pageInfo);
		Limit limitInsert = null;
		Limit limitDelete = null;
		Limit limitUpdate = null;
		int mId = moduleService.selectByPath("Module/selectAllindex");
		int uId = user.getuId();
		int rId = user.getuRId();
		int insertlId = (limitInsert = limitService.selectlIdByMIdAndType(mId, "����", "1")) == null ? 0
				: limitInsert.getlId();
		int deletelId = (limitDelete = limitService.selectlIdByMIdAndType(mId, "ɾ��", "1")) == null ? 0
				: limitDelete.getlId();
		int updatelId = (limitUpdate = limitService.selectlIdByMIdAndType(mId, "�޸�", "1")) == null ? 0
				: limitUpdate.getlId();
		HashMap<String, Object> hashMap = new HashMap<>();
		if (userRolModualLimitService.selectForeign(uId, rId, mId, insertlId) != 0)
			hashMap.put("insert", limitInsert);
		if (userRolModualLimitService.selectForeign(uId, rId, mId, deletelId) != 0)
			hashMap.put("delete", limitDelete);
		if (userRolModualLimitService.selectForeign(uId, rId, mId, updatelId) != 0)
			hashMap.put("update", limitUpdate);
		request.setAttribute("limitMap", hashMap);
		request.setAttribute("goParam", "indexAdmin.jsp");
		return "indexSuperadmin";
	}
}
