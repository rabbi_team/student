package com.zlplax.student.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zlplax.student.entity.Limit;
import com.zlplax.student.entity.Module;
import com.zlplax.student.entity.User;
import com.zlplax.student.entity.UserRolModualLimit;
import com.zlplax.student.service.LimitService;
import com.zlplax.student.service.ModuleService;
import com.zlplax.student.service.UserRolModualLimitService;
import com.zlplax.student.service.UserService;

@Controller
@RequestMapping("User")
public class UserController {
	@Autowired
	UserService userService;
	@Autowired
	ModuleService moduleService;
	@Autowired
	UserRolModualLimitService userRolModualLimitService;
	@Autowired
	LimitService limitService;

	@RequestMapping("login")
	@ResponseBody
	public String login(String username, String password, HttpSession session) {
		User user = userService.selectUUsername(username);
		if (user == null || user.getuStatus().equals("2"))
			return "2";
		if (!user.getuPassword().equals(password))
			return "3";
		session.setAttribute("user", user);
		return "1";
	}

	@RequestMapping("insert")
	@ResponseBody
	public String insertByUser(User user) {
		int ret = userService.insertByUser(user);
		if (ret != 0)
			return "success";
		else
			return "error";
	}

	@RequestMapping("delete")
	@ResponseBody
	public String deleteByUId(int uId) {
		int ret = userService.deleteById(uId);
		if (ret != 0)
			return "success";
		else
			return "error";
	}

	@RequestMapping("update")
	@ResponseBody
	public String updateByUser(User user) {
		int ret = userService.updateByUser(user);
		if (ret != 0)
			return "success";
		else
			return "error";
	}

	@RequestMapping("selectOne")
	@ResponseBody
	public User selectOne(int uId) {
		return userService.selectOne(uId);
	}

	@RequestMapping("selectAll")
	public String selectAll(HttpServletRequest request, HttpSession session) {
		User user = (User) session.getAttribute("user");
		List<Integer> list = userRolModualLimitService.selectMIdByStatusAndUid("1", user.getuId());
		if (list.size() == 0)
			list = null;
		request.setAttribute("modules", moduleService
				.selectAllByUIdArray(list));
		String pageStr = request.getParameter("curPage");
		int curPage = pageStr == null ? 1 : !pageStr.matches("\\d+") ? 1 : Integer.parseInt(pageStr);
		PageHelper.startPage(curPage, 5);
		PageHelper.orderBy("u_id desc");
		PageInfo<User> pageInfo = new PageInfo<>(userService.selectAll());
		Limit limitInsert = null;
		Limit limitDelete = null;
		Limit limitUpdate = null;
		int mId = moduleService.selectByPath("User/selectAll");
		int uId = user.getuId();
		int rId = user.getuRId();
		int insertlId = (limitInsert = limitService.selectlIdByMIdAndType(mId, "����", "1")) == null ? 0
				: limitInsert.getlId();
		int deletelId = (limitDelete = limitService.selectlIdByMIdAndType(mId, "ɾ��", "1")) == null ? 0
				: limitDelete.getlId();
		int updatelId = (limitUpdate = limitService.selectlIdByMIdAndType(mId, "�޸�", "1")) == null ? 0
				: limitUpdate.getlId();
		HashMap<String, Object> hashMap = new HashMap<>();
		if (userRolModualLimitService.selectForeign(uId, rId, mId, insertlId) != 0)
			hashMap.put("insert", limitInsert);
		if (userRolModualLimitService.selectForeign(uId, rId, mId, deletelId) != 0)
			hashMap.put("delete", limitDelete);
		if (userRolModualLimitService.selectForeign(uId, rId, mId, updatelId) != 0)
			hashMap.put("update", limitUpdate);
		request.setAttribute("limitMap", hashMap);
		request.setAttribute("pageInfo", pageInfo);
		request.setAttribute("goParam", "generalUser.jsp");
		return "indexSuperadmin";
	}

	@RequestMapping("uStatus")
	public String selectByStatus(String uStatus, HttpServletRequest request) {
		request.setAttribute("modules", moduleService.selectAllByStatus("1"));
		String pageStr = request.getParameter("curPage");
		int curPage = pageStr == null ? 1 : !pageStr.matches("\\d+") ? 1 : Integer.parseInt(pageStr);
		PageHelper.startPage(curPage, 5);
		PageHelper.orderBy("u_id desc");
		PageInfo<User> pageInfo = new PageInfo<>(userService.selectAllByStatus(uStatus));
		request.setAttribute("pageInfo", pageInfo);
		request.setAttribute("goParam", "generalUser.jsp");
		return "indexSuperadmin";
	}
}
