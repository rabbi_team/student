package com.zlplax.student.service;

import java.util.List;

import com.zlplax.student.entity.Rol;

public interface RolService {

	public int insertByRol(Rol rol);

	public int deleteById(int rId);

	public int updateByRol(Rol rol);

	public Rol selectOne(int rId);

	public List<Rol> selectAllByRStatus(String rStatus);

	public List<Rol> selectAll();
}
