package com.zlplax.student.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zlplax.student.entity.Limit;

public interface LimitService {

	public int insertByLimit(Limit limit);

	public int deleteById(int lId);

	public int updateByLimit(Limit limit);

	public Limit selectOne(int lId);

	public Limit selectlIdByMIdAndType(int mId, String type, String status);

	public List<Limit> selectAllByStatus(String lStatus);

	public List<Limit> selectAll();
}
