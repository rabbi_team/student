package com.zlplax.student.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zlplax.student.entity.UserRolModualLimit;
import com.zlplax.student.dao.UserRolModualLimitDAO;
import com.zlplax.student.service.UserRolModualLimitService;

@Service
public class UserRolModualLimitServiceImpl implements UserRolModualLimitService {
	@Autowired
	UserRolModualLimitDAO userRolModualLimitDAO;

	public int insertByUserRolModualLimit(UserRolModualLimit userRolModualLimit) {
		return userRolModualLimitDAO.insertByUserRolModualLimit(userRolModualLimit);
	}

	public int deleteById(int urmlId) {
		return userRolModualLimitDAO.deleteById(urmlId);
	}

	public int updateByUserRolModualLimit(UserRolModualLimit userRolModualLimit) {
		return userRolModualLimitDAO.updateByUserRolModualLimit(userRolModualLimit);
	}

	public UserRolModualLimit selectOne(int urmlId) {
		return userRolModualLimitDAO.selectOne(urmlId);
	}

	public List<UserRolModualLimit> selectAll() {
		return userRolModualLimitDAO.selectAll();
	}

	@Override
	public List<Integer> selectMIdByStatusAndUid(String status, int uId) {
		return userRolModualLimitDAO.selectMIdByStatusAndUid(status, uId);
	}

	@Override
	public int selectForeign(int uId, int rId, int mId, int lId) {
		return userRolModualLimitDAO.selectForeign(uId, rId, mId, lId);
	}
}
