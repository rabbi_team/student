package com.zlplax.student.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zlplax.student.entity.Module;
import com.zlplax.student.dao.ModuleDAO;
import com.zlplax.student.service.ModuleService;

@Service
public class ModuleServiceImpl implements ModuleService {
	@Autowired
	ModuleDAO moduleDAO;

	public int insertByModule(Module module) {
		return moduleDAO.insertByModule(module);
	}

	public int deleteById(int mId) {
		return moduleDAO.deleteById(mId);
	}

	public int updateByModule(Module module) {
		return moduleDAO.updateByModule(module);
	}

	public Module selectOne(int mId) {
		return moduleDAO.selectOne(mId);
	}

	public List<Module> selectAll() {
		return moduleDAO.selectAll();
	}

	@Override
	public List<Module> selectAllByStatus(String mStatus) {
		return moduleDAO.selectAllByStatus(mStatus);
	}

	@Override
	public List<Module> selectAllByUIdArray(List<Integer> intArr) {
		return moduleDAO.selectAllByUIdArray(intArr);
	}

	@Override
	public int selectByPath(String pathUrl) {
		return moduleDAO.selectByPath(pathUrl);
	}
}
