package com.zlplax.student.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zlplax.student.entity.Limit;
import com.zlplax.student.dao.LimitDAO;
import com.zlplax.student.service.LimitService;

@Service
public class LimitServiceImpl implements LimitService {
	@Autowired
	LimitDAO limitDAO;

	public int insertByLimit(Limit limit) {
		return limitDAO.insertByLimit(limit);
	}

	public int deleteById(int lId) {
		return limitDAO.deleteById(lId);
	}

	public int updateByLimit(Limit limit) {
		return limitDAO.updateByLimit(limit);
	}

	public Limit selectOne(int lId) {
		return limitDAO.selectOne(lId);
	}

	public List<Limit> selectAll() {
		return limitDAO.selectAll();
	}

	@Override
	public List<Limit> selectAllByStatus(String lStatus) {
		return limitDAO.selectAllByStatus(lStatus);
	}

	@Override
	public Limit selectlIdByMIdAndType(int mId, String type, String status) {
		return limitDAO.selectlIdByMIdAndType(mId, type, status);
	}
}
