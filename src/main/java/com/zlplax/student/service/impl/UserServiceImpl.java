package com.zlplax.student.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zlplax.student.entity.User;
import com.zlplax.student.dao.UserDAO;
import com.zlplax.student.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	UserDAO userDAO;

	public int insertByUser(User user) {
		return userDAO.insertByUser(user);
	}

	public int deleteById(int uId) {
		return userDAO.deleteById(uId);
	}

	public int updateByUser(User user) {
		return userDAO.updateByUser(user);
	}

	public User selectOne(int uId) {
		return userDAO.selectOne(uId);
	}

	public List<User> selectAll() {
		return userDAO.selectAll();
	}

	@Override
	public List<User> selectAllByStatus(String uStatus) {
		return userDAO.selectAllByStatus(uStatus);
	}

	@Override
	public int selectCountByURId(int rId) {
		return userDAO.selectCountByURId(rId);
	}

	@Override
	public User selectUUsername(String username) {
		return userDAO.selectUUsername(username);
	}
}
