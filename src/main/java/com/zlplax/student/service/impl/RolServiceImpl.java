package com.zlplax.student.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zlplax.student.entity.Rol;
import com.zlplax.student.dao.RolDAO;
import com.zlplax.student.service.RolService;

@Service
public class RolServiceImpl implements RolService {
	@Autowired
	RolDAO rolDAO;

	public int insertByRol(Rol rol) {
		return rolDAO.insertByRol(rol);
	}

	public int deleteById(int rId) {
		return rolDAO.deleteById(rId);
	}

	public int updateByRol(Rol rol) {
		return rolDAO.updateByRol(rol);
	}

	public Rol selectOne(int rId) {
		return rolDAO.selectOne(rId);
	}

	public List<Rol> selectAll() {
		return rolDAO.selectAll();
	}

	@Override
	public List<Rol> selectAllByRStatus(String rStatus) {
		return rolDAO.selectAllByRStatus(rStatus);
	}
}
