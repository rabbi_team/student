package com.zlplax.student.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zlplax.student.entity.Grade;
import com.zlplax.student.dao.GradeDAO;
import com.zlplax.student.service.GradeService;

@Service
public class GradeServiceImpl implements GradeService {
	@Autowired
	GradeDAO gradeDAO;

	public int insertByGrade(Grade grade) {
		return gradeDAO.insertByGrade(grade);
	}

	public int deleteById(int gId) {
		return gradeDAO.deleteById(gId);
	}

	public int updateByGrade(Grade grade) {
		return gradeDAO.updateByGrade(grade);
	}

	public Grade selectOne(int gId) {
		return gradeDAO.selectOne(gId);
	}

	public List<Grade> selectAll(){
		return gradeDAO.selectAll();
	}
}
