package com.zlplax.student.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zlplax.student.entity.User;

public interface UserService {

	public int insertByUser(User user);

	public int deleteById(int uId);

	public int updateByUser(User user);

	public int selectCountByURId(int rId);

	public User selectOne(int uId);

	public User selectUUsername(@Param("username") String username);

	public List<User> selectAllByStatus(String uStatus);

	public List<User> selectAll();
}
