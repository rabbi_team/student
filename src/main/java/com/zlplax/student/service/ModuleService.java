package com.zlplax.student.service;

import java.util.List;

import com.zlplax.student.entity.Module;

public interface ModuleService {

	public int insertByModule(Module module);

	public int deleteById(int mId);

	public int updateByModule(Module module);

	public Module selectOne(int mId);

	public int selectByPath(String pathUrl);

	public List<Module> selectAllByUIdArray(List<Integer> intArr);

	public List<Module> selectAllByStatus(String mStatus);

	public List<Module> selectAll();
}
