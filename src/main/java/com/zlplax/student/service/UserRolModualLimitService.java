package com.zlplax.student.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zlplax.student.entity.UserRolModualLimit;

public interface UserRolModualLimitService {

	public int insertByUserRolModualLimit(UserRolModualLimit userRolModualLimit);

	public int deleteById(int urmlId);

	public int updateByUserRolModualLimit(UserRolModualLimit userRolModualLimit);

	public UserRolModualLimit selectOne(int urmlId);

	public int selectForeign(int uId, int rId, int mId, int lId);

	public List<Integer> selectMIdByStatusAndUid(String status, @Param("uId") int uId);

	public List<UserRolModualLimit> selectAll();
}
