package com.zlplax.student.service;

import java.util.List;

import com.zlplax.student.entity.Grade;

public interface GradeService {

	public int insertByGrade(Grade grade);

	public int deleteById(int gId);

	public int updateByGrade(Grade grade);

	public Grade selectOne(int gId);

	public List<Grade> selectAll();
}
